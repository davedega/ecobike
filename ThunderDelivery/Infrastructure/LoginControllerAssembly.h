//
//  LoginControllerAssembly.h
//  ThunderDelivery
//
//  Created by david delgado on 9/11/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "TyphoonAssembly.h"
#import "LoginViewController.h"

@interface LoginControllerAssembly : TyphoonAssembly
- (LoginViewController *)loginViewController;
- (id<ILoginUseCase>)loginUseCase;
@end
