//
//  LoginControllerAssembly.m
//  ThunderDelivery
//
//  Created by david delgado on 9/11/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "LoginControllerAssembly.h"
#import "LoginUseCaseImpl.h"
#import "LoginViewController.h"

@implementation LoginControllerAssembly
- (LoginViewController *)loginViewController{
    return [TyphoonDefinition withClass:[LoginViewController class] configuration:^(TyphoonDefinition *definition)
            
            {
                
                [definition injectProperty:@selector(loginUseCase) with:[self loginUseCase]];
                
            }];
}
- (id<ILoginUseCase>)loginUseCase{
    return [TyphoonDefinition withClass:[LoginUseCaseImpl class]];
}
@end
