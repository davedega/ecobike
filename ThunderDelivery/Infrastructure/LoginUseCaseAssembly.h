//
//  LoginUseCaseAssembly.h
//  ThunderDelivery
//
//  Created by david delgado on 9/11/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "TyphoonAssembly.h"
#import "LoginUseCaseImpl.h"
#import "IloginService.h"

@interface LoginUseCaseAssembly : TyphoonAssembly
- (LoginUseCaseImpl *)loginUseCaseImpl;
- (id<ILoginService>)loginService;
@end
