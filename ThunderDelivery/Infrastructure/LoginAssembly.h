//
//  LoginAssembly.h
//  ThunderDelivery
//
//  Created by david delgado on 9/11/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "TyphoonAssembly.h"
#import "LoginViewController.h"
#import "LoginUseCaseImpl.h"
#import "IloginService.h"



@interface LoginAssembly : TyphoonAssembly
#pragma mark - bind de LoginViewController con ILoginUseCase
- (LoginViewController *)loginViewController;
- (id<ILoginUseCase>)loginUseCase;
#pragma mark - bind de LoginUseCaseImpl con ILoginService
- (id<ILoginService>)loginService;
@end
