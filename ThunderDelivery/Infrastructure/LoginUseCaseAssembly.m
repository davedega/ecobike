//
//  LoginUseCaseAssembly.m
//  ThunderDelivery
//
//  Created by david delgado on 9/11/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "LoginUseCaseAssembly.h"
#import "LoginServiceImpl.h"

@implementation LoginUseCaseAssembly
- (LoginUseCaseImpl *)loginUseCaseImpl{
    return [TyphoonDefinition withClass:[LoginUseCaseImpl class] configuration:^(TyphoonDefinition *definition)
            
            {
                
                [definition injectProperty:@selector(loginService) with:[self loginService]];
                
            }];
}
- (id<ILoginService>)loginService{
    return [TyphoonDefinition withClass:[LoginServiceImpl class]];
}
@end
