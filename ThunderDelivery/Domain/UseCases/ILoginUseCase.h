//
//  ILoginUseCase.h
//  ThunderDelivery
//
//  Created by david delgado on 9/9/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ILoginUseCase <NSObject>

-(BOOL)loginWith:(NSString*)username:(NSString*)andPassword;

@end
