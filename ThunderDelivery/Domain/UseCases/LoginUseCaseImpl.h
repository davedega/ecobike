//
//  LoginUseCaseImpl.h
//  ThunderDelivery
//
//  Created by david delgado on 9/10/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILoginUseCase.h"
#import "ILoginService.h"

@interface LoginUseCaseImpl : NSObject<ILoginUseCase>

@property(nonatomic, strong) id <ILoginService> loginService;

@end
