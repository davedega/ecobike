//
//  LoginViewController.h
//  ThunderDelivery
//
//  Created by david delgado on 9/1/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILoginViewModelListener.h"
#import "ILoginUseCase.h"

@interface LoginViewController : UIViewController <ILoginViewModelListener>
@property(nonatomic, strong) id <ILoginUseCase> loginUseCase;
@end
