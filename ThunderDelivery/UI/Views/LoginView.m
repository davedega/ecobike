//
//  LoginView.m
//  ThunderDelivery
//
//  Created by david delgado on 9/1/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "LoginView.h"
#import "ILoginViewModelListener.h"

@implementation LoginView

id<ILoginViewModelListener> mListener;

@synthesize username, password;

-(NSString*)getUsername{
    return [username text];
}

-(NSString*)getPassword{
    return [password text];
}

-(void)showWelcomeMessage{
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Thunder!"
                                                       message:@"Bienvenido!!"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    [theAlert show];

}
-(void)showBadAccesMessage{
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Thunder"
                                                       message:@"Credenciales Incorrectas"
                                                      delegate:self
                                             cancelButtonTitle:@"Cerrar"
                                             otherButtonTitles:nil];
    [theAlert show];
}
-(void)setListener:(id<ILoginViewModelListener>)listener{
    mListener = listener;
}

- (IBAction)loginPressed:(id)sender {
    [mListener doLogin];

}

@end
