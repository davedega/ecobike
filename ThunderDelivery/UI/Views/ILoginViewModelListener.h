//
//  ILoginViewModelListener.h
//  ThunderDelivery
//
//  Created by david delgado on 9/2/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ILoginViewModelListener <NSObject>
-(void)doLogin;
@end
